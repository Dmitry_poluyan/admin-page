'use strict';
var log = require('../libs/log')(module);
var UserModel = require('../models/user');
var GroupModel = require('../models/group');

function find(array, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] === value){
            return i;
        }
    }
    return -1;
}

module.exports.addUser = function (req, res, next) {
    var newUser = new UserModel({
        userName: req.body.userName,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email
    });
    newUser.trySave(function (err) {
        if (err) {
            return next(err);
        }
        log.info('New user created!');
        return res.status(201).json({status: 'OK', user: newUser});
    });
};

module.exports.allUsersForPagination = function (req, res, next) {
    UserModel.find({}, null, {skip: req.query.skipPage, limit: req.query.itemsOnPage}).then(function (users) {
        return UserModel.count({}).then(function (count) {
            return res.json({status: 'OK', users: users, count: count});
        });
    }).catch(next);
};

module.exports.updateUser = function (req, res, next) {
    UserModel.findById(req.params._id).then(function (user) {
        if (!user) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        user.userName = req.body.userName;
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.email = req.body.email;

        return user.trySave(function (err) {
            if (err) {
                return next(err);
            }
            log.info('User updated!');
            return res.json({status: 'OK', user: user});
        });
    }).catch(next);
};

module.exports.addGroupForUser = function (req, res, next) {
    var err;
    if (!req.params.groupId || !req.params.userId) {
        log.info('Fields empty');
        err = new Error('Fields empty');
        err.status = 422;
        return next(err);
    }
    var newGroupId = req.params.groupId;
    var userId = req.params.userId;

    UserModel.findById(userId).then(function (user) {
        if (!user) {
            err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        var groupsId = user.groupId;
        var checkUserGroup = find(groupsId, newGroupId);

        if (checkUserGroup !== -1) {
            log.info('The user is already in this group');
            err = new Error('The user is already in this group');
            err.status = 422;
            return next(err);
        }
        groupsId.push(newGroupId);
        user.groupId = groupsId;

        return user.save(function (err) {
            if (err) {
                return next(err);
            }
            log.info('User updated!');
            return res.json({status: 'OK', user: user});
        });
    }).catch(next);
};

module.exports.groupsUser = function (req, res, next) {
    UserModel.findOne({userName: req.params.userName}).then(function (user) {
        if (!user) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        return user.groupId;
    }).then(function (groupsId) {
        return GroupModel.find({_id: { $in: groupsId }}).then(function (groups) {
            return res.json({status: 'OK', groups: groups});
        });
    }).catch(next);
};

module.exports.usersGroup = function (req, res, next) {
    UserModel.find({groupId: req.params._id}).then(function (users) {
        return res.json({status: 'OK', users: users});
    }).catch(next);
};

module.exports.usersByUserName = function (req, res, next) {
    UserModel.findOne({userName: req.params.userName}).then(function (user) {
        if (!user) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        log.info('Search user by user name:');
        return res.json({status: 'OK', user: user});
    }).catch(next);
};

module.exports.deleteUser = function (req, res, next) {
    UserModel.findById(req.params._id).then(function (user) {
        if (!user) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        return user.remove(function (err) {
            if (err) {
                return next(err);
            }
            log.info('User removed!');
            return res.send({status: 'OK'});
        });
    }).catch(next);
};

module.exports.deleteGroupFromUser = function (req, res, next) {
    if (!req.params.groupId || !req.params.userId) {
        log.info('Fields empty');
        var err = new Error('Fields empty');
        err.status = 422;
        return next(err);
    }
    var userId = req.params.userId;
    var groupId = req.params.groupId;

    UserModel.update({_id: userId},{$pull: {groupId: groupId}}).then(function (user) {
        return res.json({status: 'OK', users: user});
    }).catch(next);
};
