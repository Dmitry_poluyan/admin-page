'use strict';
var log = require('../libs/log')(module);
var GroupModel = require('../models/group');
var UserModel = require('../models/user');

module.exports.addGroup = function (req, res, next) {
    var newGroup = new GroupModel({
        groupName: req.body.groupName,
        groupTitle: req.body.groupTitle
    });
    newGroup.trySave(function (err) {
        if (err) {
            return next(err);
        } else {
            log.info('New group created!');
            return res.status(201).json({status: 'OK', group: newGroup});
        }
    });
};

module.exports.allGroupsForPagination = function (req, res, next) {
    GroupModel.find({}, null, {skip: req.query.skipPage, limit: req.query.itemsOnPage}).then(function (groups) {
        return GroupModel.count({}).then(function (count) {
            return res.json({status: 'OK', groups: groups, count: count});
        });
    }).catch(next);
};

module.exports.allGroups = function (req, res, next) {
    GroupModel.find({}).then(function (groups) {
        return res.json({status: 'OK', groups: groups});
    }).catch(next);
};

module.exports.updateGroup = function (req, res, next) {
    GroupModel.findById(req.params._id).then(function (group) {
        if (!group) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        group.groupName = req.body.groupName;
        group.groupTitle = req.body.groupTitle;

        group.trySave(function (err) {
            if (err) {
                return next(err);
            }
            log.info('Group updated!');
            return res.json({status: 'OK', group: group});
        });
    }).catch(next);
};

module.exports.groupsByGroupName = function (req, res, next) {
    GroupModel.findOne({groupName: req.params.groupName}).then(function (group) {
        if (!group) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        log.info('Search group by group name:');
        return res.json({status: 'OK', group: group});
    }).catch(next);
};

module.exports.deleteGroup = function (req, res, next) {
    GroupModel.findById(req.params._id).then(function (group) {
        if (!group) {
            var err = new Error('Not found');
            err.status = 404;
            return next(err);
        }
        return group.remove(function (err, group) {
            if (err) {
                return next(err);
            }
            return group._id;
        });
    }).then(function (groupId) {
        return UserModel.update({groupId: groupId},{$pull: {groupId: groupId}},{ multi: true }).then(function (users) {
            return res.json({status: 'OK', users: users});
        });
    }).catch(next);
};