'use strict';
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var Schema = mongoose.Schema;

var Group = new Schema({
    groupName: {
        type: String,
        unique: 'The group name exists',
        required: 'The group name required',
        lowercase: true,
        trim: true,
        validate: [/^[a-zA-Z0-9-.]{4,20}$/,'You must enter: 4 ~ 20 characters (Latin alphabet, digits, ".", "-")']
    },
    groupTitle:{
        type: String,
        trim: true,
        minlength: 6,
        maxlength: 20,
        required: 'The group title required'
    }
});

Group.plugin(require('mongoose-beautiful-unique-validation'));

var GroupModel = mongoose.model('Group', Group);

module.exports = GroupModel;