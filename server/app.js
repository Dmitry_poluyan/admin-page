'use strict';
var express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path'),
    methodOverride = require('method-override'),
    dbConnect = require('./libs/mongoose'),
    config = require('./config'),
    log = require('./libs/log')(module);

var app = express();

var groups = require('./routes/groups');
var users = require('./routes/users');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(express.static(path.join(__dirname+'../../public/app')));

app.use('/api/groups', groups);
app.use('/api/users', users);

// error handlers
app.use(function(err, req, res, next){
    if(err.name === 'ValidationError'){
        err.status = 422;
    }
    log.error('%s %d %s', req.method, err.status ? err.status : req.statusCode, err.message);
    return res.status(err.status ? err.status : 500).json({
        nameError: err.name,
        messageError: err.message ? err.message : 'Error',
        validationError: err.errors,
        growlMessages:[{'text': err.message, 'tittle': err.name, 'severity': 'warning'}]});
});

app.listen(config.get('port'), function(){
    log.info('sever run ' + config.get('port'));
});
module.exports = app;