'use strict';
var app = require('../app');

var chai = require('chai');
var expect = chai.expect;
var should = chai.should();
var assert  = chai.assert ;
var supertest = require('supertest');
var mocha = require('mocha');

var shortId = require('shortid');

var groupName = shortId()+'N';
var groupTitle = shortId()+'T';
var groupId;

var forUpdateGroup = 'UPD'+shortId();

var userNameU = shortId()+'N';
var firstNameU = 'firstNameU';
var lastNameU = 'lastNameU';
var emailU = shortId()+'@mail.ru';
var userId;

var forUpdateUser = shortId();
var forUpdateUserEmail = shortId()+'@mail.ru';

var optionsForpagination = {
    currentPage: 0,
    itemsOnPage: 5,
    skipPage: 0
};

describe('Groups', function() {


    it('should add new group', function (done) {
        supertest(app)
            .post('/api/groups/')
            .send({
                groupName: groupName,
                groupTitle: groupTitle
            })
            .expect(function(res) {
                res.body.group.groupTitle = groupTitle;
                res.body.group.groupName = groupName.toLowerCase();
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var group = res.body.group;
                groupId = group._id;

                //assert
                assert.property(group, 'groupTitle');
                assert.property(group, 'groupName');
                assert.property(group, '_id');

                assert.typeOf(group, 'object');

                assert.equal(group.groupTitle, groupTitle);
                assert.equal(group.groupName, groupName.toLowerCase());

                //expect
                expect(group).to.have.property('groupTitle');
                expect(group.groupTitle).to.equal(groupTitle);

                //should
                group.should.be.a('object');

                group.groupTitle.should.equal(groupTitle);

                group.should.have.property('groupTitle');
                group.should.have.property('groupName');
                group.should.have.property('_id');
                done();
            });
    });
    it('should list groups for pagination', function (done) {
        supertest(app)
            .get('/api/groups/groupsForPagination')
            .expect(200)
            .send(optionsForpagination)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var groups = res.body.groups;

                assert.typeOf(groups, 'array');

                assert.property(groups[0], 'groupTitle');
                assert.property(groups[0], 'groupName');
                assert.property(groups[0], '_id');

                done();
            });
    });
    it('should list all groups', function (done) {
        supertest(app)
            .get('/api/groups/')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var groups = res.body.groups;
                assert.typeOf(groups, 'array');

                assert.property(groups[0], 'groupTitle');
                assert.property(groups[0], 'groupName');
                assert.property(groups[0], '_id');
                done();
            });
    });
    it('should group by group name', function (done) {
        supertest(app)
            .get('/api/groups/'+ groupName.toLowerCase())
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var group = res.body.group;
                assert.property(group, 'groupTitle');
                assert.property(group, 'groupName');
                assert.property(group, '_id');

                assert.typeOf(group, 'object');

                assert.equal(group.groupTitle, groupTitle);
                assert.equal(group.groupName, groupName.toLowerCase());

                done();
            });
    });

    it('should edit group', function (done) {
        supertest(app)
            .put('/api/groups/'+ groupId)
            .send({
                groupName: forUpdateGroup.toLowerCase(),
                groupTitle: forUpdateGroup
            })
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var group = res.body.group;

                assert.property(group, 'groupTitle');
                assert.property(group, 'groupName');
                assert.property(group, '_id');

                assert.typeOf(group, 'object');

                assert.equal(group.groupTitle, forUpdateGroup);
                assert.equal(group.groupName, forUpdateGroup.toLowerCase());

                done();
            });
    });

    it('should list users this group', function (done) {
        supertest(app)
            .get('/api/users/usersGroup/'+ groupId)
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var users = res.body.users;

                assert.typeOf(users, 'array');

                done();
            });
    });

    //it('should delete group', function (done) {
    //    supertest(app)
    //        .delete('/api/groups/'+ groupId)
    //        .expect(200)
    //        .end(function (err, res) {
    //            if (err) {
    //                return done(err);
    //            }
    //            var body = res.body;
    //
    //            assert.property(body, 'status');
    //
    //            assert.typeOf(body, 'object');
    //
    //            assert.equal(body.status, 'OK');
    //
    //            done();
    //        });
    //});

});
describe('Users', function() {

    it('should add new user', function (done) {
        supertest(app)
            .post('/api/users/')
            .send({
                userName: userNameU,
                firstName: firstNameU,
                lastName: lastNameU,
                email: emailU
            })
            .expect(201)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var user = res.body.user;
                userId = user._id;

                assert.property(user, 'userName');
                assert.property(user, 'firstName');
                assert.property(user, 'lastName');
                assert.property(user, 'email');
                assert.property(user, '_id');
                assert.property(user, 'groupId');

                assert.typeOf(user, 'object');

                assert.equal(user.userName, userNameU.toLowerCase());
                assert.equal(user.firstName, firstNameU);
                assert.equal(user.lastName, lastNameU);
                assert.equal(user.email, emailU.toLowerCase());

                done();
            });
    });
    it('should list all users', function (done) {
        supertest(app)
            .get('/api/users/')
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var users = res.body.users;

                assert.typeOf(users, 'array');

                assert.property(users[0], 'userName');
                assert.property(users[0], 'firstName');
                assert.property(users[0], 'lastName');
                assert.property(users[0], 'email');
                assert.property(users[0], '_id');
                assert.property(users[0], 'groupId');

                done();
            });
    });
    it('should list groups this user', function (done) {
        supertest(app)
            .get('/api/users/groupsUser/'+userNameU.toLowerCase())
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var groups = res.body.groups;

                assert.typeOf(groups, 'array');

                done();
            });
    });

    it('should user by username', function (done) {
        supertest(app)
            .get('/api/users/'+ userNameU.toLowerCase())
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var user = res.body.user;

                assert.property(user, 'userName');
                assert.property(user, 'firstName');
                assert.property(user, 'lastName');
                assert.property(user, 'email');
                assert.property(user, '_id');
                assert.property(user, 'groupId');

                assert.typeOf(user, 'object');

                assert.equal(user.userName, userNameU.toLowerCase());
                assert.equal(user.firstName, firstNameU);
                assert.equal(user.lastName, lastNameU);
                assert.equal(user.email, emailU.toLowerCase());

                done();
            });
    });

    it('should edit user', function (done) {
        supertest(app)
            .put('/api/users/'+ userId)
            .send({
                userName: forUpdateUser,
                firstName: firstNameU,
                lastName: lastNameU,
                email: forUpdateUserEmail
            })
            .expect(200)
            .end(function (err, res) {
                if (err) {
                    return done(err);
                }
                var user = res.body.user;

                assert.property(user, 'userName');
                assert.property(user, 'firstName');
                assert.property(user, 'lastName');
                assert.property(user, 'email');
                assert.property(user, '_id');
                assert.property(user, 'groupId');

                assert.typeOf(user, 'object');

                assert.equal(user.userName, forUpdateUser.toLowerCase());
                assert.equal(user.firstName, firstNameU);
                assert.equal(user.lastName, lastNameU);
                assert.equal(user.email, forUpdateUserEmail.toLowerCase());

                done();
            });
    });
    //
    //it('should delete user', function (done) {
    //    supertest(app)
    //        .delete('/api/users/'+ userId)
    //        .expect(200)
    //        .end(function (err, res) {
    //            if (err) {
    //                return done(err);
    //            }
    //            var body = res.body;
    //
    //            assert.property(body, 'status');
    //
    //            assert.typeOf(body, 'object');
    //
    //            assert.equal(body.status, 'OK');
    //
    //            done();
    //        });
    //});

});