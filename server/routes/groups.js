'use strict';
var groupsCtrl = require('../controllers/group.js');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/groupsForPagination', groupsCtrl.allGroupsForPagination);
router.get('/', groupsCtrl.allGroups);
router.get('/:groupName', groupsCtrl.groupsByGroupName);

router.post('/', groupsCtrl.addGroup);

router.put('/:_id', groupsCtrl.updateGroup);

router.delete('/:_id', groupsCtrl.deleteGroup);

module.exports = router;