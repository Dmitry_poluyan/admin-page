'use strict';
var usersCtrl = require('../controllers/user.js');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
    log.info('Time: ', Date.now());
    next();
});

router.get('/', usersCtrl.allUsersForPagination);
router.get('/:userName', usersCtrl.usersByUserName);
router.get('/groupsUser/:userName', usersCtrl.groupsUser);
router.get('/usersGroup/:_id', usersCtrl.usersGroup);

router.post('/', usersCtrl.addUser);

router.put('/:_id', usersCtrl.updateUser);
router.put('/addGroup/:userId/:groupId', usersCtrl.addGroupForUser);
router.put('/deleteGroup/:userId/:groupId', usersCtrl.deleteGroupFromUser);

router.delete('/:_id', usersCtrl.deleteUser);

module.exports = router;