'use strict';
var gulp   = require( 'gulp' );
var server = require( 'gulp-develop-server' );
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var templateCache = require('gulp-angular-templatecache');
var sourcemaps = require('gulp-sourcemaps');
var cssnano = require('gulp-cssnano');
var rimraf = require('rimraf');

gulp.task('js',['clean'], function () {
    gulp.src(['../public/app.js',
        '../public/controllers/*.js',
        '../public/router/*.js',
        '../public/services/*.js',
        '../public/directives/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('../public/app/js/'));
});

gulp.task('tmpl',['clean'], function () {
    gulp.src('../public/view/*.html')
        .pipe(templateCache({module: 'adminPage'}))
        .pipe(gulp.dest('../public/app/js/'));
});

gulp.task('css',['clean'], function () {
    gulp.src(['../public/vendor/css/*.css'])
        .pipe(sourcemaps.init())
        .pipe(concat('all.css'))
        .pipe(cssnano())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('../public/app/css/'));
});

gulp.task('clean', function (cb) {
    rimraf('../public/app/', cb);
});

gulp.task('bower:js',['clean'], function () {
    gulp.src(['../public/vendor/bower_components/angular/angular.min.js',
        '../public/vendor/bower_components/angular-ui-router/release/angular-ui-router.min.js',
        '../public/vendor/bower_components/angular-breadcrumb/dist/angular-breadcrumb.min.js',
        '../public/vendor/bower_components/angular-messages/angular-messages.min.js',
        '../public/vendor/bower_components/angular-growl-v2/build/angular-growl.min.js'])
        .pipe(concat('bower.js'))
        .pipe(gulp.dest('../public/app/js/'));
});

gulp.task('bower:css',['clean'], function () {
    gulp.src(['../public/vendor/bower_components/bootstrap/dist/css/bootstrap.min.css',
        '../public/vendor/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
        '../public/vendor/bower_components/angular-growl-v2/build/angular-growl.min.css'])
        .pipe(concat('bower.css'))
        .pipe(gulp.dest('../public/app/css/'));
});

gulp.task('copy:index',['clean'], function () {
    gulp.src(['../public/*.html'])
        .pipe(gulp.dest('../public/app/'));
});

// run server
gulp.task( 'server:start', function() {
    server.listen( { path: './app.js' } );
});

gulp.task('watch', function() {
    gulp.watch(['../public/app.js',
        '../public/controllers/*.js',
        '../public/router/*.js',
        '../public/services/*.js',
        '../public/view/*.html',
        '../public/directives/*.js',
        '../public/vendor/css/*.css'], ['build']);
});

gulp.task('build', ['clean','js', 'tmpl', 'css', 'copy:index','bower:css','bower:js']);
