'use strict';
(function () {
    angular
        .module('adminPage')
        .config(['$stateProvider', '$urlRouterProvider','$locationProvider', configLink]);

    function configLink ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/users');

        $stateProvider
            .state('usersPage', {
                url: '/users',
                templateUrl: 'usersPage.html',
                controller:'UsersPageCtrl',
                controllerAs: 'usersPage',
                ncyBreadcrumb: {
                    label: 'Users'
                }
            })
            .state('usersPage.addNewUser', {
                url: '/user',
                templateUrl: 'usersPage-addUser.html',
                ncyBreadcrumb: {
                    label: 'Add new user'
                }
            })
            .state('userEditPage', {
                url: '/users/:userName',
                templateUrl: 'userEditPage.html',
                controller:'UserEditCtrl',
                controllerAs: 'userEdit',
                ncyBreadcrumb: {
                    label: '{{userEdit.userName}}',
                    parent: 'usersPage'
                }
            })
            .state('userEditPage.addGroupForUser', {
                url: '/group',
                templateUrl: 'userEditPage-addGForU.html',
                ncyBreadcrumb: {
                    label: 'Add group for user'
                }
            })
            .state('groupsPage', {
                url: '/groups',
                templateUrl: 'groupsPage.html',
                controller:'GroupsPageCtrl',
                controllerAs: 'groupsPage',
                ncyBreadcrumb: {
                    label: 'Groups'
                }
            })
            .state('groupsPage.addNewGroup', {
                url: '/group',
                templateUrl: 'groupsPage-addGroup.html',
                ncyBreadcrumb: {
                    label: 'Add new group'
                }
            })
            .state('groupEditPage', {
                url: '/groups/:groupName',
                templateUrl: 'groupEditPage.html',
                controller:'GroupEditCtrl',
                controllerAs: 'groupEdit',
                ncyBreadcrumb: {
                    label: '{{groupEdit.groupName}}',
                    parent: 'groupsPage'
                }
            })
            .state('groupEditPage.users', {
                url: '/users',
                templateUrl: 'groupEditPage-users.html',
                ncyBreadcrumb: {
                    label: 'users'
                }
            });
    }
})();