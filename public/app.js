'use strict';
angular.module('adminPage', ['ui.router','ngMessages','angular-growl','ncy-angular-breadcrumb'])
    .config(['growlProvider', '$httpProvider', growlProviders]);

function growlProviders(growlProvider, $httpProvider) {
    growlProvider.globalTimeToLive(3000);
    growlProvider.messagesKey('growlMessages');
    growlProvider.messageTextKey('text');
    growlProvider.messageTitleKey('tittle');
    growlProvider.messageSeverityKey('severity');
    $httpProvider.interceptors.push(growlProvider.serverMessagesInterceptor);
}