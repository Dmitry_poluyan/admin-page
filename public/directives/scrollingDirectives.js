'use strict';
(function () {
    angular
        .module('adminPage')
        .directive('whenScrolled', function() {
            return function(scope, elm, attr) {
                var raw = elm[0];
                elm.bind('scroll', function() {
                    if (raw.scrollTop + raw.offsetHeight + 20>= raw.scrollHeight) {
                        scope.$apply(attr.whenScrolled);
                    }
                });
            };
        });
})();
