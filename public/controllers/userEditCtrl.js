'use strict';
(function () {
    angular
        .module('adminPage')
        .controller('UserEditCtrl',['$log','$state','$stateParams','growl','userService','groupService', UserEditCtrl]);

    function UserEditCtrl ($log, $state, $stateParams, growl, userService, groupService) {
        var self = this;

        self.userName = $stateParams.userName;
        self.showUserGroups = true;

        loadUserByUserName(self.userName);
        getGroups();

        function loadUserByUserName(userName){
            userService.userByUserName(userName).then(function(user){
                self.user = user;
            }).catch(function(rej){
                $log.debug('UserEditCtrl: loadUserByUserName: userByUserName: ERROR');
                $log.debug(rej);
                $state.go('usersPage');
            }).then(function () {
                getUserGroups(userName);
            }).catch(function(rej){
                $log.debug('UserEditCtrl: loadUserGroups: getUserGroups: ERROR');
                $log.debug(rej);
            });
        }

        function getUserGroups(userName){
            return userService.getUserGroups(userName).then(function(groups){
                if(groups.length !== 0){
                    self.groupsUser = groups;
                    self.showUserGroups = true;
                }else{
                    self.showUserGroups = false;
                }
            });
        }

        function getGroups () {
            groupService.getAllGroups().then(function(groups){
                self.groups = groups;
            }).catch(function(rej){
                $log.debug('UserEditCtrl: loadGroups: getAllGroups: ERROR');
                $log.debug(rej);
            });
        }

        self.userEdit = function (userDetails, isvalid, form) {
            if (isvalid) {
                userService.editUser(userDetails).then(function(){
                    growl.success('User edit success.', {title: 'Success!'});
                    $state.go('usersPage');
                }).catch(function(rej){
                    $log.debug('UserEditCtrl: userEdit: editUser: ERROR');
                    $log.debug(rej);
                    if(rej.data.validationError){
                        var err = rej.data.validationError;
                        if(err.userName){
                            self.validMessage = err.userName.message;
                            form.userName.$setValidity('unique', false);
                            self.change = function (form) {
                                form.userName.$setValidity('unique', true);
                            };
                        }
                        if(err.email){
                            self.validMessage = err.email.message;
                            form.email.$setValidity('unique', false);
                            self.change = function (form) {
                                form.email.$setValidity('unique', true);
                            };
                        }
                    }
                });
            }
        };

        self.addGroupForUser = function (details, isvalid, userId, form) {
            if (isvalid) {
                userService.addGroupForUser(details.group._id, userId).then(function(){
                    growl.success('Group add success.', {title: 'Success!'});
                    getUserGroups(self.userName);
                    $state.go('userEditPage');
                }).catch(function(rej){
                    $log.debug('UserEditCtrl: addGroupForUser: addGroupForUser: ERROR');
                    $log.debug(rej);
                    if(rej.data.messageError === 'The user is already in this group'){
                        self.validMessage = rej.data.messageError;
                        form.groups.$setValidity('unique', false);
                        self.change = function (form) {
                            form.groups.$setValidity('unique', true);
                        };
                    }
                });
            }
        };

        self.deleteUser = function (id) {
            if(window.confirm('Are you sure you want to delete the user?')){
                userService.deleteUser(id).then(function () {
                    $state.go('usersPage');
                    growl.success('User deleted', {title: 'Success!'});
                }).catch(function(rej){
                    $log.debug('UserEditCtrl: deleteUser: deleteUser ERROR');
                    $log.debug(rej);
                });
            }
        };
        
        self.deleteGroupFromUser = function (userId, groupId) {
            if(window.confirm('Are you sure you want to delete the group?')){
                userService.deleteGroupFromUser(userId, groupId).then(function () {
                    getUserGroups(self.userName);
                    growl.success('User group deleted', {title: 'Success!'});
                }).catch(function(rej){
                    $log.debug('UserEditCtrl: deleteGroupFromUser: deleteGroupFromUser ERROR');
                    $log.debug(rej);
                });
            }
        };

    }

})();