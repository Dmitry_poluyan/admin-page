'use strict';
(function () {
    angular
        .module('adminPage')
        .controller('GroupsPageCtrl',['$log','$state','growl','groupService','scrollingService', GroupsPageCtrl]);

    function GroupsPageCtrl ($log, $state, growl, groupService, scrollingService) {
        var self = this;

        self.showGroups = true;

        scrollingService.setPaginationOptions(0,0,5);
        var options = scrollingService.getPaginationOptions();

        loadGroups();
        function loadGroups(){
            groupService.getAllGroupsForPagination(options).then(function(data){
                if(data.groups.length !== 0){
                    self.groups = [];
                    angular.forEach(data.groups, function (val) {
                        self.groups.push(val);
                    });
                    self.showGroups = true;
                }else{
                    self.showGroups = false;
                }
                scrollingService.setCountLinks(data.count);
            }).catch(function(rej){
                $log.debug('GroupsPageCtrl: loadGroups: getAllGroupsForPagination: ERROR');
                $log.debug(rej);
            });
        }

        self.showNumPage = function () {
            if(options.currentPage < scrollingService.getTotalPagesNum()-1){
                options.currentPage = ++options.currentPage;
                options = scrollingService.getPageLinks();
                groupService.getAllGroupsForPagination(options).then(function(data){
                    angular.forEach(data.groups, function (val) {
                        self.groups.push(val);
                    });
                }).catch(function(rej){
                    $log.debug('GroupsPageCtrl: showNumPage: getAllGroupsForPagination:  ERROR');
                    $log.debug(rej);
                });
            }
        };

        self.addNewGroup = function (groupDetails, isvalid, form) {
            if (isvalid) {
                groupService.addNewGroup(groupDetails).then(function(group){
                    growl.success('Group add success.', {title: 'Success!'});
                    form.$setPristine();
                    form.$setUntouched();
                    self.newGroup ={};
                    loadGroups();
                    $state.go('groupsPage');
                }).catch(function(rej){
                    $log.debug('GroupsPageCtrl: addNewGroup: addNewGroup: ERROR');
                    $log.debug(rej);
                    if(rej.data.validationError){
                        var err = rej.data.validationError;
                        if(err.groupName){
                            self.validMessage = err.groupName.message;
                            form.groupName.$setValidity('unique', false);
                            self.change = function (form) {
                                form.groupName.$setValidity('unique', true);
                            };
                        }
                    }
                });
            }
        };


    }

})();