'use strict';
(function () {
    angular
        .module('adminPage')
        .controller('UsersPageCtrl',['$log','$state','growl','userService','paginationService', UsersPageCtrl]);

    function UsersPageCtrl ($log, $state, growl, userService, paginationService) {
        var self = this;

        self.showUsers = true;

        paginationService.setPaginationOptions(0,0,5);
        var options = paginationService.getPaginationOptions();
        self.currentPages = options.currentPage;

        self.showNumPage = function (numPage) {
            if(numPage !== self.currentPages){
                var options = paginationService.getPaginationOptions(numPage);
                self.currentPages = options.currentPage;
                $log.debug(options);
                userService.getAllUsersForPagination(options).then(function(data){
                    if(data.users.length !== 0){
                        self.users = data.users;
                        self.showUsers = true;
                    }else{
                        self.showUsers = false;
                    }
                    paginationService.setCountLinks(data.count);
                    self.paginationList = paginationService.getPaginationList();
                }).catch(function(rej){
                    $log.debug('UsersPageCtrl: loadUsers: getAllUsersForPagination: ERROR');
                    $log.debug(rej);
                });
            }
        };
        self.showNumPage();

        self.addNewUser = function (userDetails, isvalid, form) {
            if (isvalid) {
                userService.addNewUser(userDetails).then(function(){
                    growl.success('User add success.', {title: 'Success!'});
                    self.showNumPage();
                    form.$setPristine();
                    form.$setUntouched();
                    self.newUser ={};
                    $state.go('usersPage');
                }).catch(function(rej){
                    $log.debug('UsersPageCtrl: addNewUser: addNewUser: ERROR');
                    $log.debug(rej);
                    if(rej.data.validationError){
                        var err = rej.data.validationError;
                        if(err.userName){
                            self.validMessage = err.userName.message;
                            form.userName.$setValidity('unique', false);
                            self.change = function (form) {
                                form.userName.$setValidity('unique', true);
                            };
                        }
                        if(err.email){
                            self.validMessage = err.email.message;
                            form.email.$setValidity('unique', false);
                            self.change = function (form) {
                                form.email.$setValidity('unique', true);
                            };
                        }
                    }
                });
            }
        };

    }
})();