'use strict';
(function () {
    angular
        .module('adminPage')
        .controller('GroupEditCtrl',['$log','$state','$stateParams', 'growl','groupService', GroupEditCtrl]);

    function GroupEditCtrl ($log, $state, $stateParams, growl, groupService) {
        var self = this;

        self.groupName = $stateParams.groupName;
        self.state = $state;
        self.showUsersGroup = true;

        loadGroupByGroupName(self.groupName);

        function loadGroupByGroupName(groupName){
            groupService.groupByGroupName(groupName).then(function(group){
                self.group = group;
                return group._id;
            }).catch(function(rej){
                $log.debug('GroupEditCtrl: loadGroupByGroupName: groupByGroupName: ERROR');
                $log.debug(rej);
                $state.go('groupsPage');
            }).then(function (groupId) {
                return groupService.getUsersGroup(groupId).then(function(users){
                    if(users.length !== 0){
                        self.usersGroup = users;
                        self.showUsersGroup = true;
                    }else{
                        self.showUsersGroup = false;
                    }
                });
            }).catch(function(rej){
                $log.debug('GroupEditCtrl: loadUsersGroup: getUsersGroup: ERROR');
                $log.debug(rej);
            });
        }

        self.groupEdit = function (groupDetails, isvalid, form) {
            if (isvalid) {
                groupService.editGroup(groupDetails).then(function(){
                    growl.success('Group edit success.', {title: 'Success!'});
                    $state.go('groupsPage');
                }).catch(function(rej){
                    $log.debug('GroupEditCtrl: groupEdit: editGroup: ERROR');
                    $log.debug(rej);
                    if(rej.data.validationError){
                        var err = rej.data.validationError;
                        if(err.groupName){
                            self.validMessage = err.groupName.message;
                            form.groupName.$setValidity('unique', false);
                            self.change = function (form) {
                                form.groupName.$setValidity('unique', true);
                            };
                        }
                    }
                });
            }
        };

        self.deleteGroup = function (id) {
            if(window.confirm('Are you sure you want to delete the group?')){
                groupService.deleteGroup(id).then(function () {
                    $state.go('groupsPage');
                    growl.success('Group deleted', {title: 'Success!'});
                }).catch(function(rej){
                    $log.debug('GroupEditCtrl: deleteGroup: deleteGroup ERROR');
                    $log.debug(rej);
                });
            }
        };

    }

})();