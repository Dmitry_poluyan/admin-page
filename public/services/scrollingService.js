'use strict';
(function () {
    angular
        .module('adminPage')
        .service('scrollingService', scrollingService);
    function scrollingService(){
        var options = {};
        var allCountItems;

        return{
            setPaginationOptions: function (currentPage, skipPage, itemsOnPage) {
                options = {
                    currentPage: currentPage,
                    skipPage: skipPage,
                    itemsOnPage: itemsOnPage
                };
            },
            setCountLinks: function (countItems) {
                allCountItems = {
                    countLinks:countItems
                };
            },
            getPaginationOptions: function () {
                return options;
            },
            getTotalPagesNum: function () {
                return Math.ceil(allCountItems.countLinks/options.itemsOnPage);
            },
            getPageLinks: function (){
                return options = {
                    currentPage: options.currentPage,
                    skipPage: options.itemsOnPage*options.currentPage,
                    itemsOnPage: options.itemsOnPage
                };
            }
        };
    }

})();