'use strict';
(function () {
    angular
        .module('adminPage')
        .service('userService',['$http','$log', userService]);

    function userService ($http, $log) {
        this.getAllUsersForPagination = function(options) {
            return $http({
                method: 'GET',
                url: '/api/users/',
                params: options
            }).then(function(res){
                $log.debug('userService: getAllUsersForPagination: res:');
                $log.debug(res);
                return res.data;
            });
        };
        this.userByUserName = function(userName) {
            return $http({
                method: 'GET',
                url: '/api/users/'+userName
            }).then(function(res){
                $log.debug('userService: userByUserName: res:');
                $log.debug(res);
                return res.data.user;
            });
        };
        this.getUserGroups = function(userName) {
            return $http({
                method: 'GET',
                url: '/api/users/groupsUser/'+userName
            }).then(function(res){
                $log.debug('userService: getUserGroups: res:');
                $log.debug(res);
                return res.data.groups;
            });
        };
        this.editUser = function(userDetails) {
            return $http({
                method: 'PUT',
                url: '/api/users/'+ userDetails._id,
                data: userDetails
            });
        };
        this.addNewUser = function(userDetails) {
            return $http({
                method: 'POST',
                url: '/api/users/',
                data: userDetails
            });
        };
        this.addGroupForUser = function(groupId, userId) {
            return $http({
                method: 'PUT',
                url: '/api/users/addGroup/'+userId+'/'+groupId
            });
        };
        this.deleteUser = function(id) {
            return $http({
                method: 'DELETE',
                url: '/api/users/'+id
            });
        };
        this.deleteGroupFromUser = function(userId, groupId) {
            return $http({
                method: 'PUT',
                url: '/api/users/deleteGroup/'+userId+'/'+groupId
            }).then(function(res){
                $log.debug('userService: deleteGroupFromUser: res:');
                $log.debug(res);
                return res;
            });
        };
    }

})();