'use strict';
(function () {
    angular
        .module('adminPage')
        .service('groupService',['$http','$log', groupService]);

    function groupService ($http, $log) {
        this.getAllGroupsForPagination = function(options) {
            return $http({
                method: 'GET',
                url: '/api/groups/groupsForPagination',
                params: options
            }).then(function(res){
                $log.debug('groupService: getAllGroups: res:');
                $log.debug(res);
                return res.data;
            });
        };
        this.getAllGroups = function() {
            return $http({
                method: 'GET',
                url: '/api/groups/'
            }).then(function(res){
                $log.debug('groupService: getAllGroups: res:');
                $log.debug(res);
                return res.data.groups;
            });
        };
        this.groupByGroupName = function(groupName) {
            return $http({
                method: 'GET',
                url: '/api/groups/'+groupName
            }).then(function(res){
                $log.debug('groupService: groupByGroupName: res:');
                $log.debug(res);
                return res.data.group;
            });
        };
        this.getUsersGroup = function(groupId) {
            return $http({
                method: 'GET',
                url: '/api/users/usersGroup/'+groupId
            }).then(function(res){
                $log.debug('groupService: getUsersGroup: res:');
                $log.debug(res);
                return res.data.users;
            });
        };
        this.editGroup = function(groupDetails) {
            return $http({
                method: 'PUT',
                url: '/api/groups/'+ groupDetails._id,
                data: groupDetails
            });
        };
        this.addNewGroup = function(groupDetails) {
            return $http({
                method: 'POST',
                url: '/api/groups/',
                data: groupDetails
            });
        };
        this.deleteGroup = function(id) {
            return $http({
                method: 'DELETE',
                url: '/api/groups/'+id
            });
        };
    }

})();