'use strict';
(function () {
    angular
        .module('adminPage')
        .service('paginationService', paginationService);
    function paginationService(){
        var options = {};
        var allCountItems;

        function getTotalPagesNum() {
            return Math.ceil(allCountItems.countLinks/options.itemsOnPage);
        }

        function getPrevPageLinks() {
            var prevPageNum = options.currentPage - 1;
            if ( prevPageNum < 0 ) {
                prevPageNum = 0;
            }
            return prevPageNum;
        }

        function getNextPageLinks() {
            var nextPageNum = options.currentPage + 1;
            var pagesNum = getTotalPagesNum();
            if ( nextPageNum >= pagesNum ) {
                nextPageNum = pagesNum - 1;
            }
            return nextPageNum;
        }

        function nextNumPage(num){
            if ( num === 'prev' ) {
                num = getPrevPageLinks();
            }
            if ( num === 'next' ) {
                num = getNextPageLinks();
            }else{
                num = angular.isUndefined(num)?0:num;
            }
            options = {
                currentPage: num,
                skipPage: options.itemsOnPage*num,
                itemsOnPage: options.itemsOnPage
            };
        }

        return{
            setPaginationOptions: function (currentPage, skipPage, itemsOnPage) {
                options = {
                    currentPage: currentPage,
                    skipPage: skipPage,
                    itemsOnPage: itemsOnPage
                };
            },
            setCountLinks: function (countItems) {
                allCountItems = {
                    countLinks:countItems
                };
            },
            getPaginationOptions: function (num) {
                if(num || num === 0){
                    nextNumPage(num);
                }
                return options;
            },
            getPaginationList: function () {
                var pagesNum = getTotalPagesNum();
                var paginationList= [];
                paginationList.push({
                    name: 'prev',
                    link:'prev'
                });
                for(var i = 0; i < pagesNum; i++){
                    var nameNum = i+1;
                    paginationList.push({
                        name: nameNum,
                        link: i
                    });
                }
                paginationList.push({
                    name: 'next',
                    link:'next'
                });

                if(pagesNum>1){
                    return paginationList;
                }else{
                    return false;
                }
            }
        };
    }

})();